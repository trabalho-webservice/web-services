package com.backend.trainelog.server.repository;

import com.backend.trainelog.server.model.DiaSemanal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiaSemanalRepository extends JpaRepository<DiaSemanal, Long> {

}
