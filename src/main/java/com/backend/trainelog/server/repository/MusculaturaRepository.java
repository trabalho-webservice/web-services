package com.backend.trainelog.server.repository;

import com.backend.trainelog.server.model.Musculatura;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MusculaturaRepository extends JpaRepository<Musculatura, Long> {

}

