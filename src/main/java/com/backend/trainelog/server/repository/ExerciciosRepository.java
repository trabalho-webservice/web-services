package com.backend.trainelog.server.repository;

import com.backend.trainelog.server.model.Exercicios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExerciciosRepository extends JpaRepository<Exercicios, Long> {
  List<Exercicios> findByMusculaturaIdMusculatura(Long idMusculatura);
}
