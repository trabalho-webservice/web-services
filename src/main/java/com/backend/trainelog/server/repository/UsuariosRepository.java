package com.backend.trainelog.server.repository;

import com.backend.trainelog.server.model.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuariosRepository extends JpaRepository<Usuarios, Long> {

}
