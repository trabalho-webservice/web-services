package com.backend.trainelog.server.repository;
import com.backend.trainelog.server.model.Fichas;

import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FichasRepository extends JpaRepository<Fichas, Long> {
    List<Fichas> findByUsuario_IdUserAndDiaSemanal_IdDia(Long idUser, Long idDia);
    void deleteByUsuario_IdUserAndDiaSemanal_IdDia(Long idUser, Long idDia);
}
