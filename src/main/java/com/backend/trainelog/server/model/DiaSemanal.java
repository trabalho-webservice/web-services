package com.backend.trainelog.server.model;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DiaSemanal")
@Getter
@Setter
public class DiaSemanal {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long idDia;

  @NotNull
  private String nomeDia;
}
