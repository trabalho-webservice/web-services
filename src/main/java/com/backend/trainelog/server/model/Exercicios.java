package com.backend.trainelog.server.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "exercicios")
@Getter
@Setter
public class Exercicios {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_exercicio;

    @NotNull
    private String nome_exercicio;

    @ManyToOne
    @JoinColumn(name = "id_musculatura", referencedColumnName = "idMusculatura")
    private Musculatura musculatura;

}
