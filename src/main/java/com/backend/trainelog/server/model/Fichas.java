package com.backend.trainelog.server.model;

import java.util.List;
import jakarta.validation.constraints.NotNull;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "fichas")
@Getter
@Setter
public class Fichas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFicha;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_user", referencedColumnName = "idUser")
    private Usuarios usuario;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_dia", referencedColumnName = "idDia")
    private DiaSemanal diaSemanal;

    @NotNull
    @ManyToMany
    @JoinTable(
        name = "ficha_exercicios",
        joinColumns = @JoinColumn(name = "id_ficha"),
        inverseJoinColumns = @JoinColumn(name = "id_exercicio")
    )
    private List<Exercicios> exercicios;

    @NotNull
    @ElementCollection
    @CollectionTable(name = "ficha_series", joinColumns = @JoinColumn(name = "id_ficha"))
    @Column(name = "serie")
    private List<Integer> series;

    @NotNull
    @ElementCollection
    @CollectionTable(name = "ficha_repeticoes", joinColumns = @JoinColumn(name = "id_ficha"))
    @Column(name = "repeticao")
    private List<Integer> repeticoes;
}
