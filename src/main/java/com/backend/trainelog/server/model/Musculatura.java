package com.backend.trainelog.server.model;

import jakarta.validation.constraints.NotNull;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/* import java.util.List; */
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "musculatura")
@Getter
@Setter
public class Musculatura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMusculatura;

    @NotNull
    private String nome_musculatura;

    @JsonIgnore
    @OneToMany(mappedBy = "musculatura")
    private List<Exercicios> exercicios;
}
