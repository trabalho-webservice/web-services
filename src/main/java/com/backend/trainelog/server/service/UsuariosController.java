package com.backend.trainelog.server.service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backend.trainelog.server.model.Usuarios;
import com.backend.trainelog.server.repository.UsuariosRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import java.util.Optional;
import io.swagger.v3.oas.annotations.Operation;

import java.util.List;


    @RestController
    @RequestMapping(value = "/api/users")
    public class UsuariosController {


    @Autowired
    private final UsuariosRepository repository;

    UsuariosController(UsuariosRepository repository) {
        this.repository = repository;
    }

    // Método HTTP GET - Listar Todos os Usuários
    @Operation(summary = "Endpoint P/ Listar Todos os Usuários")
    @GetMapping("/all")
    List<Usuarios> all() {
        return repository.findAll();
    }
    
    // Método HTTP POST - Salvar Novos Usuários
    @Operation(summary = "Endpoint P/ Cadastrar Novos Usuários")
    @PostMapping("/signup")
    @NotNull
    Usuarios newUsuario(@RequestBody(required=true) Usuarios newUsuario) {
        return repository.save(newUsuario);
    }

    // Método HTTP GET - Visualizar usuários por ID
    @Operation(summary = "Endpoint P/ Visualizar Usuário por ID")
    @GetMapping("/{id}")
    Usuarios one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(null);
       // Exceção não implementada.   
    } 

    // Método HTTP PUT - Atualiza Dados dos Usuários por ID
    @Operation(summary = "Endpoint P/ Atualizar Dados dos Usuários")
    @PutMapping("/update")
    Usuarios atualizarUsuarios(@RequestBody(required=true) Usuarios atualizarUsuarios) {
        Usuarios usuarioExistente = repository.findById(atualizarUsuarios.getIdUser()).orElse(null);
        if (usuarioExistente != null) {
            // Isso vai verificar qual campo o usuário vai querer modificar

            if (atualizarUsuarios.getNomeUser() != null && atualizarUsuarios.getNomeUser() != ""){
                usuarioExistente.setNomeUser(atualizarUsuarios.getNomeUser());   
            }
            
            if (atualizarUsuarios.getEmailUser() != null && atualizarUsuarios.getEmailUser() != ""){
                usuarioExistente.setEmailUser(atualizarUsuarios.getEmailUser());                
            }

            if (atualizarUsuarios.getSenhaUser() != null && atualizarUsuarios.getSenhaUser() != "") {
                usuarioExistente.setSenhaUser(atualizarUsuarios.getSenhaUser());
            }

            return repository.save(usuarioExistente);
        } else {
            // Usuário não encontrado
            return null;
        }
    }

    @Operation(summary = "Endpoint P/ Deletar Usuário")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUsuario(@PathVariable Long id) {
        Optional<Usuarios> usuario = repository.findById(id);
        if (usuario.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }    
    //  Posteriormente, será implementado lógica de controle de acesso (email, senha)
    
}
