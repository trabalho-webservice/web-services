package com.backend.trainelog.server.service;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.trainelog.server.model.Fichas;
import com.backend.trainelog.server.repository.FichasRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import io.swagger.v3.oas.annotations.Operation;

import java.util.List;

@RestController
@RequestMapping(value = "/api/train")
public class FichasController {

  @Autowired
  private final FichasRepository ficha;

  FichasController(FichasRepository ficha) {
      this.ficha = ficha;
  }

  // Método HTTP GET - Listar Todos as Fichas
  @Operation(summary = "Endpoint P/ Listar Todas as Fichas de Todos os Usuários")
  @GetMapping("/all")
  List<Fichas> all() {
      return ficha.findAll();
  }

  // Método HTTP POST - Cadastrar Novas Fichas
  @Operation(summary = "Endpoint P/ Cadastrar Novas Fichas")
  @PostMapping("/register")
  @NotNull
  Fichas newFicha(@RequestBody(required=true) Fichas newFicha) {
    if (newFicha.getExercicios().size() != newFicha.getSeries().size() ||
          newFicha.getExercicios().size() != newFicha.getRepeticoes().size()) {
          throw new IllegalArgumentException("O número de exercícios, séries e repetições devem ser iguais.");
      } else{
        return ficha.save(newFicha);
      }   
     }

  // Método HTTP GET - Visualizar Ficha de um Usuário P/ Dia
  @Operation(summary = "Endpoint P/ Visualizar Ficha de um Usuário Em um Determinado Dia da Semana")
  @GetMapping("/user/{idUser}/dia/{idDia}")
  ResponseEntity<List<Fichas>> getFichaByUserIdAndDiaId(@PathVariable Long idUser, @PathVariable Long idDia) {
    List<Fichas> fichas = ficha.findByUsuario_IdUserAndDiaSemanal_IdDia(idUser, idDia);
    if (fichas.isEmpty()) {
        return ResponseEntity.noContent().build();
    }
    return ResponseEntity.ok(fichas);
}

  // Método HTTP PUT - Atualizar Ficha
    @Operation(summary = "Endpoint P/ Atualizar Exercício")
    @PutMapping("/update")
    Fichas atualizarFicha(@RequestBody(required=true) Fichas atualizarFicha) {
        Fichas fichaExistente = ficha.findById(atualizarFicha.getIdFicha()).orElse(null);
        if (fichaExistente != null) {
      // Isso vai verificar qual campo o usuário vai querer modificar
    
      if (atualizarFicha.getUsuario() != null){
      fichaExistente.setUsuario(null); 
      }
      if (atualizarFicha.getDiaSemanal() != null){
        fichaExistente.setDiaSemanal(null); 
      }
      if (atualizarFicha.getExercicios() != null){
        fichaExistente.setExercicios(null);
      }
      if (atualizarFicha.getSeries() != null) {
        fichaExistente.setSeries(fichaExistente.getSeries());
    }
      if (atualizarFicha.getRepeticoes() != null) {
        fichaExistente.setRepeticoes(fichaExistente.getRepeticoes());
    }
      return ficha.save(fichaExistente);
        } else {
      // Ficha não encontrada
      return null;
        }
    }
  // Método HTTP DELETE - Deletar Ficha por ID
  @Operation(summary = "Endpoint P/ Deletar Dia")
  @DeleteMapping("/delete/user/{idUser}/dia/{idDia}")
  ResponseEntity<Void> deleteFichaByUserIdAndDiaId(@PathVariable Long idUser, @PathVariable Long idDia) {
    List<Fichas> fichas = ficha.findByUsuario_IdUserAndDiaSemanal_IdDia(idUser, idDia);
    if (fichas.isEmpty()) {
        return ResponseEntity.notFound().build();
    }
    ficha.deleteByUsuario_IdUserAndDiaSemanal_IdDia(idUser, idDia);
    return ResponseEntity.noContent().build();
}
}
