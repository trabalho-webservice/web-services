package com.backend.trainelog.server.service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.trainelog.server.model.Exercicios;
import com.backend.trainelog.server.model.Musculatura;
import com.backend.trainelog.server.repository.ExerciciosRepository;
import com.backend.trainelog.server.repository.MusculaturaRepository;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Optional;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;

@RestController
@RequestMapping(value = "/api/exercise")
public class ExerciciosController {
  
  @Autowired
  private final ExerciciosRepository exercicio;

  @Autowired
  private MusculaturaRepository musculaturaRepository;

  ExerciciosController(ExerciciosRepository exercicio, MusculaturaRepository musculaturaRepository) {
      this.exercicio = exercicio;
      this.musculaturaRepository = musculaturaRepository;
  }

    // Método HTTP GET - Listar Todos os Exercícios
    @Operation(summary = "Endpoint P/ Listar Todos os Exercícios")
    @GetMapping("/all")
    List<Exercicios> all() {
        return exercicio.findAll();
    }
  
    // Método HTTP POST - Cadastrar Novos Exercícios
    @Operation(summary = "Endpoint P/ Cadastrar Novos Exercícios")
    @PostMapping("/register")
    @NotNull
    public ResponseEntity<?> registerExercicio(@RequestBody @Valid Exercicios newExercicio) {
        try {
            // Verifica se a musculatura já existe
            Musculatura musculatura = musculaturaRepository.findById(newExercicio.getMusculatura().getIdMusculatura())
                                                           .orElseThrow(() -> new RuntimeException("Musculatura não encontrada"));
            newExercicio.setMusculatura(musculatura);

            Exercicios savedExercicio = exercicio.save(newExercicio);
            return ResponseEntity.ok(savedExercicio);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
  
    // Método HTTP GET - Visualizar Exercício por ID
    @Operation(summary = "Endpoint P/ Visualizar Exercício por ID")
    @GetMapping("/{id_exercicio}")
    Exercicios one(@PathVariable Long id_exercicio) {
        return exercicio.findById(id_exercicio).orElseThrow(null);
    }

    // Método HTTP GET - Listar Exercícios por Musculatura
    @Operation(summary = "Endpoint P/ Listar Exercícios por Musculatura")
    @GetMapping("/musculatura/{id_musculatura}")
    public List<Exercicios> getExerciciosByMusculatura(@PathVariable Long id_musculatura) {
      return exercicio.findByMusculaturaIdMusculatura(id_musculatura);
    }
    
    // Método HTTP PUT - Atualizar Exercício
    @Operation(summary = "Endpoint P/ Atualizar Exercício")
    @PutMapping("/update")
    Exercicios atualizarExercicios(@RequestBody(required=true) Exercicios atualizarExercicio) {
        Exercicios exercicioExistente = exercicio.findById(atualizarExercicio.getId_exercicio()).orElse(null);
        if (exercicioExistente != null) {
      // Isso vai verificar qual campo o usuário vai querer modificar
    
            if (atualizarExercicio.getNome_exercicio() != null && atualizarExercicio.getNome_exercicio() != ""){
            exercicioExistente.setNome_exercicio(atualizarExercicio.getNome_exercicio());   
            }
            if (atualizarExercicio.getMusculatura() != null){
                exercicioExistente.setMusculatura(null); 
            }
            return exercicio.save(exercicioExistente);
            } else {
      // Dia não encontrado
            return null;
        }
    }
  
    // Método HTTP DELETE - Deleta Exercício por ID
    @Operation(summary = "Endpoint P/ Deletar Exercício por ID")
    @DeleteMapping("/delete/{id_exercicio}")
    public ResponseEntity<?> deleteMusculo(@PathVariable Long id_exercicio) {
        Optional<Exercicios> excc = exercicio.findById(id_exercicio);
        if (excc.isPresent()) {
            exercicio.deleteById(id_exercicio);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
