package com.backend.trainelog.server.service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.trainelog.server.model.DiaSemanal;
import com.backend.trainelog.server.repository.DiaSemanalRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import java.util.Optional;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;

@RestController
@RequestMapping(value = "/api/days")
public class DiasController {

  @Autowired
  private final DiaSemanalRepository diaSemanal;

  DiasController(DiaSemanalRepository diaSemanal) {
      this.diaSemanal = diaSemanal;
  }

  // Método HTTP GET - Listar Todos os Dias da Semana
  @Operation(summary = "Endpoint P/ Listar Todos os Dias da Semana")
  @GetMapping("/all")
  List<DiaSemanal> all() {
      return diaSemanal.findAll();
  }

  // Método HTTP POST - Cadastrar Novos Dias da Semana
  @Operation(summary = "Endpoint P/ Cadastrar Novos Dias da Semana")
  @PostMapping("/register")
  @NotNull
  DiaSemanal newDia(@RequestBody(required=true) DiaSemanal newDia) {
      return diaSemanal.save(newDia);
  } 
  
  // Método HTTP GET - Visualizar Dia por ID
  @Operation(summary = "Endpoint P/ Visualizar Dia por ID")
  @GetMapping("/{id_dia}")
  DiaSemanal one(@PathVariable Long id_dia) {
      return diaSemanal.findById(id_dia).orElseThrow(null);
  }
  
  // Método HTTP PUT - Atualiza Dados dos Dias por ID
  @Operation(summary = "Endpoint P/ Atualizar Dia")
  @PutMapping("/update")
  DiaSemanal atualizarDia(@RequestBody(required=true) DiaSemanal atualizarDia) {
      DiaSemanal diaExistente = diaSemanal.findById(atualizarDia.getIdDia()).orElse(null);
      if (diaExistente != null) {
        // Isso vai verificar qual campo o usuário vai querer modificar
  
        if (atualizarDia.getNomeDia() != null && atualizarDia.getNomeDia() != ""){
          diaExistente.setNomeDia(atualizarDia.getNomeDia());   
        }
        return diaSemanal.save(diaExistente);
      } else {
    // Dia não encontrado
    return null;
      }
  }

  // Método HTTP DELETE - Deleta Dia por ID
  @Operation(summary = "Endpoint P/ Deletar Dia")
  @DeleteMapping("/delete/{id_dia}")
  public ResponseEntity<?> deleteDia(@PathVariable Long id_dia) {
      Optional<DiaSemanal> dia = diaSemanal.findById(id_dia);
      if (dia.isPresent()) {
          diaSemanal.deleteById(id_dia);
          return ResponseEntity.ok().build();
      } else {
          return ResponseEntity.notFound().build();
      }
  }
}
