package com.backend.trainelog.server.service;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backend.trainelog.server.model.Musculatura;
import com.backend.trainelog.server.repository.MusculaturaRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import java.util.Optional;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;

@RestController
@RequestMapping(value = "/api/muscle")
public class MusculaturaController {

  @Autowired
  private final MusculaturaRepository musculatura;

  MusculaturaController(MusculaturaRepository musculatura) {
      this.musculatura = musculatura;
  }

  // Método HTTP GET - Listar Todos os Nomes Musculares
  @Operation(summary = "Endpoint P/ Listar Todos os Nomes Musculares")
  @GetMapping("/all")
  List<Musculatura> all() {
      return musculatura.findAll();
  }

  // Método HTTP POST - Cadastrar Novos Músculos
  @Operation(summary = "Endpoint P/ Cadastrar Novos Músculos")
  @PostMapping("/register")
  @NotNull
  Musculatura newDia(@RequestBody(required=true) Musculatura newMusculo) {
   return musculatura.save(newMusculo);
  }   

  // Método HTTP GET - Visualizar Músculo por ID
  @Operation(summary = "Endpoint P/ Visualizar Músculo por ID")
  @GetMapping("/{id_musculatura}")
  Musculatura one(@PathVariable Long id_musculatura) {
  return musculatura.findById(id_musculatura).orElseThrow(null);
  }
  
  // Método HTTP PUT - Atualizar Músculo
  @Operation(summary = "Endpoint P/ Atualizar Músculo")
  @PutMapping("/update")
  Musculatura atualizarMusculatura(@RequestBody(required=true) Musculatura atualizarMusculo) {
      Musculatura musculoExistente = musculatura.findById(atualizarMusculo.getIdMusculatura()).orElse(null);
      if (musculoExistente != null) {
    // Isso vai verificar qual campo o usuário vai querer modificar
  
    if (atualizarMusculo.getNome_musculatura() != null && atualizarMusculo.getNome_musculatura() != ""){
    musculoExistente.setNome_musculatura(atualizarMusculo.getNome_musculatura());   
    }
    return musculatura.save(musculoExistente);
      } else {
    // Dia não encontrado
    return null;
      }
  }

  // Método HTTP DELETE - Deleta Músculo por ID
  @Operation(summary = "Endpoint P/ Deletar Músculo por ID")
  @DeleteMapping("/delete/{id_musculatura}")
  public ResponseEntity<?> deleteMusculo(@PathVariable Long id_musculatura) {
      Optional<Musculatura> musculo = musculatura.findById(id_musculatura);
      if (musculo.isPresent()) {
          musculatura.deleteById(id_musculatura);
          return ResponseEntity.ok().build();
      } else {
          return ResponseEntity.notFound().build();
      }
  } 
}
