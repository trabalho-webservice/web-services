CREATE DATABASE  IF NOT EXISTS `teste` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `teste`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: teste
-- ------------------------------------------------------
-- Server version	8.0.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dia_semanal`
--

DROP TABLE IF EXISTS `dia_semanal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dia_semanal` (
  `id_dia` bigint NOT NULL AUTO_INCREMENT,
  `nome_dia` varchar(255) NOT NULL,
  PRIMARY KEY (`id_dia`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia_semanal`
--

LOCK TABLES `dia_semanal` WRITE;
/*!40000 ALTER TABLE `dia_semanal` DISABLE KEYS */;
INSERT INTO `dia_semanal` VALUES (1,'Segunda-Feira'),(2,'Terça-Feira'),(3,'Quarta-Feira'),(4,'Quinta-Feira'),(5,'Sexta-Feira');
/*!40000 ALTER TABLE `dia_semanal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exercicios`
--

DROP TABLE IF EXISTS `exercicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exercicios` (
  `id_exercicio` bigint NOT NULL AUTO_INCREMENT,
  `nome_exercicio` varchar(255) NOT NULL,
  `id_musculatura` bigint DEFAULT NULL,
  PRIMARY KEY (`id_exercicio`),
  KEY `FKqk8ey8py7s77l4laangwcifk` (`id_musculatura`),
  CONSTRAINT `FKqk8ey8py7s77l4laangwcifk` FOREIGN KEY (`id_musculatura`) REFERENCES `musculatura` (`id_musculatura`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercicios`
--

LOCK TABLES `exercicios` WRITE;
/*!40000 ALTER TABLE `exercicios` DISABLE KEYS */;
INSERT INTO `exercicios` VALUES (1,'Agachamento Livre',1),(2,'Leg 45º',1),(3,'Cadeira Extensora',1),(4,'Agachamento Hack',1),(5,'Passada Avanço',1),(6,'Flexora Sentada',2),(7,'Flexora Deitada',2),(8,'Stiff',2),(9,'Levantamento Terra',2),(10,'Levantamento Terra Romeno',3),(11,'Elevação Pélvica',3),(12,'Agachamento Sumô',3),(13,'Cadeira Abdutora',3),(14,'Kickback - Polia',3),(15,'Elevação de Panturrilha em Pé',4),(16,'Elevação de Panturrilha Sentado',4),(17,'Panturrilha no Leg 45º',4),(18,'Supino Reto - Articulado',5),(19,'Supino Inclinado - Articulado',5),(20,'Supino Declinado - Articulado',5),(21,'Crucifixo',5),(22,'Peck Deck',5),(23,'Desenvolvimento C/ Barra',6),(24,'Desenvolvimento Arnold',6),(25,'Desenvolvimento C/ Halteres',6),(26,'Elevação Lateral',6),(27,'Elevação Frontal',6),(28,'Tríceps Corda',7),(29,'Tríceps Testa - Polia',7),(30,'Tríceps Banco',7),(31,'Kickback Tríceps',7),(32,'Remada Unilateral (Serrote)',8),(33,'Pullover',8),(34,'Barra Fixa',8),(35,'Puxada Alta Frontal',8),(36,'Remada Sentada',8),(37,'Rosca Direta',9),(38,'Rosca Alternada',9),(39,'Rosca Martelo',9),(40,'Rosca Scott',9),(41,'Rosca Inversa',10),(42,'Rosca Martelo',10),(43,'Rolamento de Punho',10),(44,'Prancha',11),(45,'Abdominal Oblíquo',11),(46,'Elevação de Perna - Barra Fixa',11),(47,'Abdominal Pedalada',11),(48,'Roda Abdominal',11);
/*!40000 ALTER TABLE `exercicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficha_exercicios`
--

DROP TABLE IF EXISTS `ficha_exercicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ficha_exercicios` (
  `id_ficha` bigint NOT NULL,
  `id_exercicio` bigint NOT NULL,
  KEY `FKjugq9suhuqs4tkrr96j4hsj4l` (`id_exercicio`),
  KEY `FKjvhnykfemhn8rsy3hemhlb809` (`id_ficha`),
  CONSTRAINT `FKjugq9suhuqs4tkrr96j4hsj4l` FOREIGN KEY (`id_exercicio`) REFERENCES `exercicios` (`id_exercicio`),
  CONSTRAINT `FKjvhnykfemhn8rsy3hemhlb809` FOREIGN KEY (`id_ficha`) REFERENCES `fichas` (`id_ficha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficha_exercicios`
--

LOCK TABLES `ficha_exercicios` WRITE;
/*!40000 ALTER TABLE `ficha_exercicios` DISABLE KEYS */;
INSERT INTO `ficha_exercicios` VALUES (1,18),(1,19),(1,20),(1,23),(1,26),(1,27),(1,28),(1,29),(1,30),(2,1),(2,2),(2,3),(2,6),(2,7),(2,8),(2,15),(2,16),(3,33),(3,34),(3,35),(3,36),(3,37),(3,38),(3,39),(3,41),(3,43),(4,19),(4,20),(4,21),(4,24),(4,26),(4,27),(4,30),(4,31),(5,3),(5,4),(5,5),(5,16),(5,17),(6,32),(6,33),(6,34),(6,35),(6,46),(6,48),(7,6),(7,7),(7,8),(7,10),(7,11),(7,12),(7,15),(7,16),(8,38),(8,37),(8,40),(8,41),(8,42),(8,24),(8,26),(8,27);
/*!40000 ALTER TABLE `ficha_exercicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficha_repeticoes`
--

DROP TABLE IF EXISTS `ficha_repeticoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ficha_repeticoes` (
  `id_ficha` bigint NOT NULL,
  `repeticao` int DEFAULT NULL,
  KEY `FKi2m5sgentu2rfvr3p9ughdx6o` (`id_ficha`),
  CONSTRAINT `FKi2m5sgentu2rfvr3p9ughdx6o` FOREIGN KEY (`id_ficha`) REFERENCES `fichas` (`id_ficha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficha_repeticoes`
--

LOCK TABLES `ficha_repeticoes` WRITE;
/*!40000 ALTER TABLE `ficha_repeticoes` DISABLE KEYS */;
INSERT INTO `ficha_repeticoes` VALUES (1,12),(1,12),(1,12),(1,12),(1,12),(1,12),(1,12),(1,12),(1,12),(2,12),(2,12),(2,12),(2,12),(2,12),(2,12),(2,20),(2,20),(3,15),(3,15),(3,15),(3,15),(3,12),(3,12),(3,12),(3,15),(3,2),(4,10),(4,10),(4,10),(4,10),(4,12),(4,12),(4,12),(4,12),(5,15),(5,15),(5,2),(5,20),(5,20),(6,8),(6,8),(6,8),(6,8),(6,10),(6,10),(7,6),(7,6),(7,6),(7,6),(7,8),(7,8),(7,15),(7,15),(8,10),(8,10),(8,10),(8,12),(8,12),(8,8),(8,8),(8,8);
/*!40000 ALTER TABLE `ficha_repeticoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficha_series`
--

DROP TABLE IF EXISTS `ficha_series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ficha_series` (
  `id_ficha` bigint NOT NULL,
  `serie` int DEFAULT NULL,
  KEY `FKo3k62ccrlb9wbhl2cwvyr0eco` (`id_ficha`),
  CONSTRAINT `FKo3k62ccrlb9wbhl2cwvyr0eco` FOREIGN KEY (`id_ficha`) REFERENCES `fichas` (`id_ficha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficha_series`
--

LOCK TABLES `ficha_series` WRITE;
/*!40000 ALTER TABLE `ficha_series` DISABLE KEYS */;
INSERT INTO `ficha_series` VALUES (1,3),(1,3),(1,3),(1,3),(1,3),(1,3),(1,3),(1,3),(1,3),(2,3),(2,3),(2,3),(2,3),(2,3),(2,3),(2,4),(2,4),(3,4),(3,4),(3,4),(3,4),(3,3),(3,3),(3,3),(3,4),(3,4),(4,4),(4,4),(4,4),(4,4),(4,3),(4,3),(4,3),(4,3),(5,4),(5,4),(5,3),(5,5),(5,5),(6,4),(6,4),(6,4),(6,4),(6,4),(6,4),(7,4),(7,4),(7,4),(7,4),(7,4),(7,4),(7,5),(7,5),(8,4),(8,4),(8,4),(8,4),(8,5),(8,5),(8,4),(8,4);
/*!40000 ALTER TABLE `ficha_series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fichas`
--

DROP TABLE IF EXISTS `fichas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fichas` (
  `id_ficha` bigint NOT NULL AUTO_INCREMENT,
  `id_dia` bigint NOT NULL,
  `id_user` bigint NOT NULL,
  PRIMARY KEY (`id_ficha`),
  KEY `FKg9xgl6gjlyv1h07bbiukbdny2` (`id_dia`),
  KEY `FKnfwpa7g2kkb6n2jtd1nlo50n6` (`id_user`),
  CONSTRAINT `FKg9xgl6gjlyv1h07bbiukbdny2` FOREIGN KEY (`id_dia`) REFERENCES `dia_semanal` (`id_dia`),
  CONSTRAINT `FKnfwpa7g2kkb6n2jtd1nlo50n6` FOREIGN KEY (`id_user`) REFERENCES `usuarios` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fichas`
--

LOCK TABLES `fichas` WRITE;
/*!40000 ALTER TABLE `fichas` DISABLE KEYS */;
INSERT INTO `fichas` VALUES (1,1,1),(2,3,1),(3,5,1),(4,1,2),(5,2,2),(6,3,2),(7,4,2),(8,5,2);
/*!40000 ALTER TABLE `fichas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musculatura`
--

DROP TABLE IF EXISTS `musculatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musculatura` (
  `id_musculatura` bigint NOT NULL AUTO_INCREMENT,
  `nome_musculatura` varchar(255) NOT NULL,
  PRIMARY KEY (`id_musculatura`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musculatura`
--

LOCK TABLES `musculatura` WRITE;
/*!40000 ALTER TABLE `musculatura` DISABLE KEYS */;
INSERT INTO `musculatura` VALUES (1,'Quadríceps'),(2,'Posterior de Coxa'),(3,'Glúteos'),(4,'Panturrilha'),(5,'Peitoral'),(6,'Ombro'),(7,'Tríceps'),(8,'Dorsal'),(9,'Bíceps'),(10,'Antebraço'),(11,'Abdômen');
/*!40000 ALTER TABLE `musculatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id_user` bigint NOT NULL AUTO_INCREMENT,
  `email_user` varchar(255) NOT NULL,
  `nome_user` varchar(255) NOT NULL,
  `senha_user` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'darllyson@gmail.com','Darllyson','123'),(2,'lucas@gmail.com','Lucas','456'),(3,'wendell@gmail.com','Wendell','789'),(4,'yan@gmail.com','Yan','987'),(5,'matheus@gmail.com','Matheus','654');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'teste'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-13  0:42:27
