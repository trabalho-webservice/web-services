## Traine Log - Backend

### Sumário

- [Resumo](#resumo)
- [Integrantes](#integrantes)
- [Tecnologias Usadas](#tecnologias-usadas)
- [Instalação](#instalação)

### Resumo

Este projeto visa implementar o framework Spring e a linguagem de programação Java para criar e desenvolver um servidor dedicado à gestão de fichas eletrônicas para exercícios de musculação. A aplicação permite aos usuários acessar e organizar suas rotinas de treino de forma eficiente e intuitiva.

### Integrantes

- 20231118878 - Francisco Lucas
- 20231118647 - Lucas Gabriel
- 20231118973 - Paulo André
- 20231119745 - Darllyson Santos
- 20231110653 - Edson Soares
- 20231110601 - Yan Victor
- 20231111076 - Guilherme de Lima
- 20231118896 - Carlos Wendell
- 20231110960 - Guilherme do Nascimento
- 20231118577 - Matheus Alexssander

### Tecnologias Usadas

Neste sistema, foram utilizadas as seguintes tecnologias e dependências:

- Java JDK 17
- Spring Framework
- Spring Data JPA
- Spring DevTools
- Spring Boot
- Spring Rest
- MySQL Server
- MySQL Driver
- Jersey
- Lombok
- Apache Maven
- Swagger UI

### Instalação

Para instalar e executar este projeto localmente, siga estas etapas:

1. Clone o repositório em sua máquina local.
2. Certifique-se de ter o Java JDK 17 instalado.
3. Instale o Apache Maven para gerenciar as dependências do projeto.
4. Configure um servidor MySQL e atualize as informações de configuração no arquivo application.properties.
5. Execute o comando mvn spring-boot:run para iniciar o servidor.
6. O servidor estará disponível em localhost:8080.
